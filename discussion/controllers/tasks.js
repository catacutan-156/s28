// [SECTION] Dependencies and Modules
	const Task = require("../models/Task");


// [SECTION] Functionalities

	// Create new task
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name;
		let newTask = new Task({
			name: taskName
		});

		return newTask.save().then((task, error) => {
			if (error) {
				return "Saving new task failed."
			} else {
				return "A new task was created!"
			}
		})
	};


	// Retrieve all tasks 
	module.exports.getAllTasks = () => {
		return Task.find({}).then(searchResult => {
			return searchResult;
		})
	};


	// Retrieve Single Task
	module.exports.getTask = (data) => {
		return Task.findById(data).then(result => {
			return result;
		});
	};


	// Update Task
	module.exports.updateTask = (taskId, newContent) => {
		let newStatus = newContent.status;
		return Task.findById(taskId).then((foundTask, error) => {
			if (foundTask) {
				foundTask.status = newStatus;
				return foundTask.save().then((updatedTask, saveErr) => {
					if (saveErr) {
						return false;
					} else {
						return updatedTask;
					}
				})
			} else {
				return "No task found.";
			}
		});
	};


	// Delete Task
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
			if (removedTask) {
				return `${removedTask} Task deleted successfully.`;
			} else {
				return "No tasks were removed!";
			}
		})
	};