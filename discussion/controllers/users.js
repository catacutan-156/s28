// [SECTION] Dependencies and Modules
	const User = require("../models/User");
	const bcrypt = require("bcrypt");

// [SECTION] User Functionalities
	
	// Create New User
	module.exports.registerUser = (reqBody) => {
		let fName = reqBody.firstName;
		let lName = reqBody.lastName;
		let email = reqBody.email;
		let passW = reqBody.password;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10)
		});

		return newUser.save().then((user, error) => {
			if (user) {
				return user;
			} else {
				return "Failed to create user.";
			}
		});
	};


	// Retrieve All Users
	module.exports.getAllUsers = () => {
		return User.find({}).then(resultOfQuery => {
			return resultOfQuery;
		})
	};


	// Retrieve a Single User
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			return result;
		});
	};


	// Delete a User
	module.exports.deleteUser = (userId) => {
		return User.findByIdAndRemove(userId).then((removedUser, error) => {
			if (removedUser) {
				return `${removedUser} Account deleted successfully.`;
			} else {
				return "No accounts were removed!";
			}
		})
	};
	

	// Update User Details
	module.exports.updateUser = (userId, newContent) => {
		let fName = newContent.firstName;
		let lName = newContent.lastName;
		return User.findById(userId).then((foundUser, error) => {
			if (foundUser) {
				foundUser.firstName = fName;
				foundUser.lastName = lName;
				return foundUser.save().then((updatedUser, saveErr) => {
					if (saveErr) {
						return false;
					} else {
						return updatedUser;
					}
				})
			} else {
				return "No user found.";
			}
		});
	};
