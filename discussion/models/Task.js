// Create a Schema, model for the task collection and make sure to export
const mongoose = require("mongoose");

const taskBlueprint = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name is required."]
	},
	status: {
		type: String,
		default: "Pending"
	}
});

module.exports = mongoose.model("Task", taskBlueprint);