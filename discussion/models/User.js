// Create the Schema, model for each user and make sure to expose the file afterwards.

const mongoose = require("mongoose");

const userBlueprint = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	// The "tasks" field will be an "array of objects", this will contain a list of tasks assigned for this user.
	tasks: [
		{
			taskId: {
				type: String,
				required: [true, "Task ID is required."]
			},
			assignedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

// Model for users
module.exports = mongoose.model("User", userBlueprint);