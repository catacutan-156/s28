// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require("../controllers/users");


// [SECTION] Routing Component
	const route = express.Router();


// [SECTION] User Routes

	// Register User
	route.post("/register", (req, res) => {
		let data = req.body;
		controller.registerUser(data).then(result => res.send(result));
	});


	// Retrieve All Users
	route.get("/", (req, res) => {
		controller.getAllUsers().then(result => {
			res.send(result);
		});
	});


	// Retrieve User Profile
	route.get("/:id", (req, res) => {
		let userId = req.params.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		});
	});


	// Delete User Profile
	route.delete("/:id", (req, res) => {
		let userId = req.params.id;
		controller.deleteUser(userId).then(outcome => {
			res.send(outcome);
		});
	});


	// Update User Details/Profile
	route.put("/:id", (req, res) => {
		let id = req.params.id;
		let katawan = req.body
		controller.updateUser(id, katawan).then(outcome => {
			res.send(outcome);
		});
	});


// [SECTION] Expose Routing System
	module.exports = route;