// [SECTION] Dependencies and Modules
	const express = require("express");
	const controller = require("../controllers/tasks");


// [SECTION] Routing Component
	const route = express.Router();


// [SECTION] Task Routes

	// Create Task
	route.post("/create", (req, res) => {
		let taskInfo = req.body;
		controller.createTask(taskInfo).then(result => res.send(result));
	});


	// Retrieve All Tasks
	route.get("/", (req, res) => {
		controller.getAllTasks().then(result => {
			res.send(result);
		})
	});


	// Retrieve Single Task
	route.get("/:id", (req, res) => {
		let taskId = req.params.id;
		controller.getTask(taskId).then(outcome => {
			res.send(outcome);
		});
	});


	// Delete Single Task
	route.delete("/:id", (req, res) => {
		let taskId = req.params.id;
		controller.deleteTask(taskId).then(outcome => {
			res.send(outcome);
		});
	});


	// Update Task
	route.put("/:id", (req, res) => {
		let id = req.params.id;
		let content = req.body;
		controller.updateTask(id, content).then(outcome => {
			res.send(outcome);
		});
	});


// [SECTION] Expose Routing System
	module.exports = route;